import pygame as pg

# Colour (R, G, B)
BLUE = (20, 30, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)


class Text():

    def __init__(self, surface, txt, pos, colour=BLACK, size=25, anchor='midtop'):
        self.surface = surface
        self.size = size
        self.text = str(txt)
        self.pos = pos
        self.anchor = anchor
        self.colour = colour
        
        self.generate_text_surface()

    def generate_text_surface(self):
        font = pg.font.Font('Fonts/Pokoljaro.otf', self.size)
        self.text_surface = font.render(self.text, True, self.colour)
        self.text_rect = self.text_surface.get_rect()
        match self.anchor:
            case 'midtop':
                self.text_rect.midtop = self.pos
            case 'topleft':
                self.text_rect.topleft = self.pos
            case 'center':
                self.text_rect.center = self.pos
        self.surface.blit(self.text_surface, self.text_rect)




class InputBox(Text):

    def __init__(self, x, y, width=300, height=40, size=45, txt=''):
        Text.__init__(self, x, y, size, txt)
        self.width = width
        self.height = height
        self.colour = BLUE

    def draw(self, screen):
        text_surface, text_rect = self.generate_text_surface()
        input_box = pg.Rect(self.centre_x, self.top_y, self.width, self.height)
        input_box.topleft = self.centre_x - (self.width / 2), self.top_y
        text_rect.center = input_box.center
        pg.draw.rect(screen, BLUE, input_box, 2)
        screen.fill(BLACK, input_box.inflate(-4, -4))
        screen.blit(text_surface, text_rect)

    def handle_event(self, event):
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_BACKSPACE:
                self.text = self.text[:-1]
            elif event.key == pg.K_RETURN:
                return
            else:
                self.text += event.unicode