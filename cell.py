import pygame

class Cell:
    def __init__(self, id, image, pos, surface, anchor='topleft', width=70, height=70):
        self.id = id
        self.image = image
        self.pos = pos
        self.surface = surface
        self.anchor = anchor
        self.width = width
        self.height = height
        
        self.value = 0
        self.state = 'blank'
        self.bg_colour = (200, 200, 200)
        self.draw_img = False
        
        self.create_cell()
    
    
    def create_cell(self):
        self.cell_rect = pygame.Rect(self.pos[0],
                                     self.pos[1],
                                     self.width,
                                     self.height)
        
        
    def get_image_position(self):
        self.image_rect = self.image.get_rect()
        match self.anchor:
            case 'center':
                self.image_rect.center = self.pos
            case 'topleft':
                self.image_rect.topleft = self.pos
            case 'midtop':
                self.image_rect.midtop = self.pos
        return self.image_rect


    def get_state(self):
        match self.state:
            case 'blank':
                self.bg_colour = (200, 200, 200)
                self.draw_img = False
            case 'assigned':
                self.bg_colour = (235, 213, 179)#
                self.draw_img = True
            case 'selected':
                self.bg_colour = (200, 200, 200)#
                self.draw_img = True
        return self.bg_colour, self.draw_img
    
    
    def draw_image(self):
        self.image_rect = self.get_image_position()
        return self.surface.blit(self.image, self.image_rect)
    
    
    def draw(self):
        self.bg_colour, self.draw_img = self.get_state()
        pygame.draw.rect(self.surface, self.bg_colour, self.cell_rect)
        if self.draw_img:
            return self.draw_image()
    
    
                    
    

