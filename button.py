import pygame


class Text:
    def __init__(self, name, pos, surface, anchor='midtop', size=25):
        self.name = name
        self.pos = pos
        self.surface = surface
        self.anchor = anchor
        self.size = size

        self.state = 'unavailable'
        self.colour = (75, 75, 75)
        
        self.create_button()
    
    
    def create_button(self):
        font = pygame.font.Font('Fonts/Pokoljaro.otf', self.size)
        self.colour = self.get_state()
        self.text = font.render(self.name, True, self.colour)
        self.text_rect = self.text.get_rect()
        match self.anchor:
            case 'center':
                self.text_rect.center = self.pos
            case 'topleft':
                self.text_rect.topleft = self.pos
            case 'midtop':
                self.text_rect.midtop = self.pos
        return self.text, self.text_rect

        
        
    def get_state(self):
        match self.state:
            case 'unavailable':
                self.colour = (75, 75, 75)
            case 'available':
                self.colour = (255, 255, 255)
            case 'highlight':
                self.colour = (0, 255, 0)
            case 'used':
                self.colour = (255, 0, 0)
        return self.colour
    
    
    def draw(self):
        self.text, self.text_rect = self.create_button()
        return self.surface.blit(self.text, self.text_rect)
    
    
class Button:
    def __init__(self, msg, pos, surface, image, anchor='midtop'):
        self.msg = msg
        self.pos = pos
        self.surface = surface
        self.anchor = anchor
        self.image=image
        self.image_rect = image.get_rect()

        self.state = 'normal'
        self.clicked = False
        self.colour = (255, 255, 255)
        
        self.create_button()
    
    
    def create_button(self):
        match self.anchor:
            case 'center':
                self.image_rect.center = self.pos
            case 'topleft':
                self.image_rect.topleft = self.pos
            case 'midtop':
                self.image_rect.midtop = self.pos
        font = pygame.font.Font('Fonts/Pokoljaro.otf', 40)
        self.colour = self.get_state()
        self.text = font.render(self.msg, True, self.colour)
        self.text_rect = self.text.get_rect()
        self.text_rect.center = self.image_rect.center
        return self.text, self.text_rect

        
    def get_state(self):
        match self.state:
            case 'normal':
                self.colour = (255, 255, 255)
            case 'highlight':
                self.colour = (0, 255, 0)
        return self.colour
    
    
    def draw(self):
        self.text, self.text_rect = self.create_button()
        self.surface.blit(self.image, self.image_rect)
        return self.surface.blit(self.text, self.text_rect)