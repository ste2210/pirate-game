"""
network.py contains Network() class for handling data transfer between client
and server
"""
import socket
import pickle

class Network:
    """
    Network class: contains methods for listening to server and sending replies
    """
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server = "127.0.0.1"
        self.port = 57344
        self.addr = (self.server, self.port)
        self.connected = self.connect()

    def connect(self):
        """
        listens for incoming data from server
        """
        try:
            self.client.connect(self.addr)
            print('connecting to server')
            data = pickle.loads(self.client.recv(8192))
            print(data)
            return data
        except EOFError as err:
            print(err)

    def send(self, data):
        """
        takes argument: 'data', pickles it and sends it to server
        """
        try:
            self.client.send(pickle.dumps(data))
            data = pickle.loads(self.client.recv(8192))
            print('Received data:', data)
            return data
        except socket.error as err:
            print(err)
