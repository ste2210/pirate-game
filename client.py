import sys
import pygame
import network
import player
import cell
import button
import text
import random

# %% Initiate pygame

# Define window size
MARGIN = 3
WIDTH = 70
HEIGHT = 70
GRID_TOT_WIDTH = (8*WIDTH + 8*MARGIN)
WINDOW_SIZE = [GRID_TOT_WIDTH*2, GRID_TOT_WIDTH]
MENU_WIDTH = (WINDOW_SIZE[0] - GRID_TOT_WIDTH)

pygame.init()
screen = pygame.display.set_mode(WINDOW_SIZE)
menu = pygame.Rect(GRID_TOT_WIDTH, 0, MENU_WIDTH, WINDOW_SIZE[1])
grid = pygame.Rect(0, 0, GRID_TOT_WIDTH, WINDOW_SIZE[1])
pygame_icon = pygame.image.load('Images/Black Icons/pirate-captain.png')
pygame.display.set_caption("Pirate Game")
pygame.display.set_icon(pygame_icon)

# %% Import images
plank = pygame.image.load('Images/Buttons/plank.png').convert_alpha()
plank = pygame.transform.smoothscale(plank, (round(WINDOW_SIZE[1] *0.6), round(WINDOW_SIZE[1] *0.2)))

steal_img_black = pygame.image.load(
    'Images/Black Icons/galleon.png').convert_alpha()
steal_img_black = pygame.transform.smoothscale(steal_img_black, (WIDTH, HEIGHT))
steal_rect_black = steal_img_black.get_rect()

kill_img_black = pygame.image.load(
    'Images/Black Icons/bowie-knife.png').convert_alpha()
kill_img_black = pygame.transform.smoothscale(kill_img_black, (WIDTH, HEIGHT))
kill_rect_black = kill_img_black.get_rect()

gift_img_black = pygame.image.load(
    'Images/Black Icons/present.png').convert_alpha()
gift_img_black = pygame.transform.smoothscale(gift_img_black, (WIDTH, HEIGHT))
gift_rect_black = gift_img_black.get_rect()

skull_img_black = pygame.image.load(
    'Images/Black Icons/skull-crossed-bones.png').convert_alpha()
skull_img_black = pygame.transform.smoothscale(skull_img_black, (WIDTH, HEIGHT))
skull_rect_black = skull_img_black.get_rect()

swap_img_black = pygame.image.load(
    'Images/Black Icons/back-forth.png').convert_alpha()
swap_img_black = pygame.transform.smoothscale(swap_img_black, (WIDTH, HEIGHT))
swap_rect_black = swap_img_black.get_rect()

choose_img_black = pygame.image.load(
    'Images/Black Icons/select.png').convert_alpha()
choose_img_black = pygame.transform.smoothscale(choose_img_black, (WIDTH, HEIGHT))
choose_rect_black = choose_img_black.get_rect()

shield_img_black = pygame.image.load(
    'Images/Black Icons/shield.png').convert_alpha()
shield_img_black = pygame.transform.smoothscale(shield_img_black, (WIDTH, HEIGHT))
shield_rect_black = shield_img_black.get_rect()

mirror_img_black = pygame.image.load(
    'Images/Black Icons/shield-reflect.png').convert_alpha()
mirror_img_black = pygame.transform.smoothscale(mirror_img_black, (WIDTH, HEIGHT))
mirror_rect_black = mirror_img_black.get_rect()

bomb_img_black = pygame.image.load(
    'Images/Black Icons/rolling-bomb.png').convert_alpha()
bomb_img_black = pygame.transform.smoothscale(bomb_img_black, (WIDTH, HEIGHT))
bomb_rect_black = bomb_img_black.get_rect()

double_img_black = pygame.image.load(
    'Images/Black Icons/two-coins.png').convert_alpha()
double_img_black = pygame.transform.smoothscale(double_img_black, (WIDTH, HEIGHT))
double_rect_black = double_img_black.get_rect()

bank_img_black = pygame.image.load(
    'Images/Black Icons/piggy-bank.png').convert_alpha()
bank_img_black = pygame.transform.smoothscale(bank_img_black, (WIDTH, HEIGHT))
bank_rect_black = bank_img_black.get_rect()

fivek_img_black = pygame.image.load('Images/coins-5000.png').convert_alpha()
fivek_img_black = pygame.transform.smoothscale(fivek_img_black, (WIDTH, HEIGHT))
fivek_rect_black = fivek_img_black.get_rect()

threek_img_black = pygame.image.load('Images/coins-3000.png').convert_alpha()
threek_img_black = pygame.transform.smoothscale(threek_img_black, (WIDTH, HEIGHT))
threek_rect_black = threek_img_black.get_rect()

onek_img_black = pygame.image.load('Images/coins-1000.png').convert_alpha()
onek_img_black = pygame.transform.smoothscale(onek_img_black, (WIDTH, HEIGHT))
onek_rect_black = onek_img_black.get_rect()

fivehund_img_black = pygame.image.load('Images/coins-200.png').convert_alpha()
fivehund_img_black = pygame.transform.smoothscale(
    fivehund_img_black, (WIDTH, HEIGHT))
fivehund_rect_black = fivehund_img_black.get_rect()

steal_img_white = pygame.image.load(
    'Images/White Icons/galley.png').convert_alpha()
steal_img_white = pygame.transform.smoothscale(steal_img_white, (WIDTH, HEIGHT))
steal_rect_white = steal_img_white.get_rect()

kill_img_white = pygame.image.load(
    'Images/White Icons/bowie-knife.png').convert_alpha()
kill_img_white = pygame.transform.smoothscale(kill_img_white, (WIDTH, HEIGHT))
kill_rect_white = kill_img_white.get_rect()

gift_img_white = pygame.image.load(
    'Images/White Icons/present.png').convert_alpha()
gift_img_white = pygame.transform.smoothscale(gift_img_white, (WIDTH, HEIGHT))
gift_rect_white = gift_img_white.get_rect()

skull_img_white = pygame.image.load(
    'Images/White Icons/skull-crossed-bones.png').convert_alpha()
skull_img_white = pygame.transform.smoothscale(skull_img_white, (WIDTH, HEIGHT))
skull_rect_white = skull_img_white.get_rect()

swap_img_white = pygame.image.load(
    'Images/White Icons/back-forth.png').convert_alpha()
swap_img_white = pygame.transform.smoothscale(swap_img_white, (WIDTH, HEIGHT))
swap_rect_white = swap_img_white.get_rect()

choose_img_white = pygame.image.load(
    'Images/White Icons/select.png').convert_alpha()
choose_img_white = pygame.transform.smoothscale(choose_img_white, (WIDTH, HEIGHT))
choose_rect_white = choose_img_white.get_rect()

shield_img_white = pygame.image.load(
    'Images/White Icons/shield.png').convert_alpha()
shield_img_white = pygame.transform.smoothscale(shield_img_white, (WIDTH, HEIGHT))
shield_rect_white = shield_img_white.get_rect()

mirror_img_white = pygame.image.load(
    'Images/White Icons/shield-reflect.png').convert_alpha()
mirror_img_white = pygame.transform.smoothscale(mirror_img_white, (WIDTH, HEIGHT))
mirror_rect_white = mirror_img_white.get_rect()

bomb_img_white = pygame.image.load(
    'Images/White Icons/rolling-bomb.png').convert_alpha()
bomb_img_white = pygame.transform.smoothscale(bomb_img_white, (WIDTH, HEIGHT))
bomb_rect_white = bomb_img_white.get_rect()

double_img_white = pygame.image.load(
    'Images/White Icons/two-coins.png').convert_alpha()
double_img_white = pygame.transform.smoothscale(double_img_white, (WIDTH, HEIGHT))
double_rect_white = double_img_white.get_rect()

bank_img_white = pygame.image.load(
    'Images/White Icons/piggy-bank.png').convert_alpha()
bank_img_white = pygame.transform.smoothscale(bank_img_white, (WIDTH, HEIGHT))
bank_rect_white = bank_img_white.get_rect()

fivek_img_white = pygame.image.load('Images/coins-5000.png').convert_alpha()
fivek_img_white = pygame.transform.smoothscale(fivek_img_white, (WIDTH, HEIGHT))
fivek_rect_white = fivek_img_white.get_rect()

threek_img_white = pygame.image.load('Images/coins-3000.png').convert_alpha()
threek_img_white = pygame.transform.smoothscale(threek_img_white, (WIDTH, HEIGHT))
threek_rect_white = threek_img_white.get_rect()

onek_img_white = pygame.image.load('Images/coins-1000.png').convert_alpha()
onek_img_white = pygame.transform.smoothscale(onek_img_white, (WIDTH, HEIGHT))
onek_rect_white = onek_img_white.get_rect()

fivehund_img_white = pygame.image.load('Images/coins-200.png').convert_alpha()
fivehund_img_white = pygame.transform.smoothscale(
    fivehund_img_white, (WIDTH, HEIGHT))
fivehund_rect_white = fivehund_img_white.get_rect()


# %% Define globals

# Define colors
L_GRAY = (200, 200, 200)
D_GRAY = (50, 50, 50)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
PARCHMENT = (235, 213, 179)

TILES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 13] + [14]*10 + [15]*25

CLOCK = pygame.time.Clock()
        
TILE_DICT = {
1: ['STEAL', steal_img_black, steal_img_white],
2: ['KILL', kill_img_black, kill_img_white],
3: ['GIFT', gift_img_black, gift_img_white],
4: ['SWAP', swap_img_black, swap_img_white],
5: ['CHOOSE', choose_img_black, choose_img_white],
6: ['SHIELD', shield_img_black, shield_img_white],
7: ['MIRROR', mirror_img_black, mirror_img_white],
8: ['DOUBLE', double_img_black, double_img_white],
9: ['BANK', bank_img_black, bank_img_white],
10: ['BOMB', bomb_img_black, bomb_img_white],
11: ['SKULL', skull_img_black, skull_img_white],
12: ['GOLD', fivek_img_black, fivek_img_white],
13: ['GOLD', threek_img_black, threek_img_white],
14: ['GOLD', onek_img_black, onek_img_white],
15: ['GOLD', fivehund_img_black, fivehund_img_white]
}

GRID = []
for row in range(7):
    for column in range(7):
        x = (MARGIN + WIDTH) * column + MARGIN + WIDTH//2
        y = (MARGIN + HEIGHT) * row + MARGIN + HEIGHT//2
        GRID.append(cell.Cell((row, column), None, (x,y), screen, width=WIDTH, height=HEIGHT))
        
INVENTORY = [button.Text(TILE_DICT[idx][0], (GRID_TOT_WIDTH + MENU_WIDTH//2, idx*WINDOW_SIZE[1]//20 + WINDOW_SIZE[1]//4), screen, size=WINDOW_SIZE[1]//30) for idx in TILE_DICT if idx < 10]

BUTTONS = {
    'TITLE': [
        button.Button('NEW GAME', (WINDOW_SIZE[0]//5, 3*WINDOW_SIZE[1]//4), screen, image=plank, anchor='center'),
        button.Button('JOIN GAME', (2.5*WINDOW_SIZE[0]//5, 3*WINDOW_SIZE[1]//4), screen, image=plank, anchor='center'),
        button.Button('QUIT GAME', (4*WINDOW_SIZE[0]//5, 3*WINDOW_SIZE[1]//4), screen, image=plank, anchor='center')
        ]
    } 


# %% Functions
def event_handler():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            if PHASE == 'intro':
                for b in BUTTONS['TITLE']:
                    if b.image_rect.collidepoint(pos):
                        b.clicked = True
            if grid.collidepoint(pos):
                for c in GRID:
                    if c.cell_rect.collidepoint(pos):
                        if PHASE == 'setup':
                            if TILES and c.value == 0:
                                c.value = TILES.pop(0)
                                c.state = 'assigned'
                                c.image = TILE_DICT[c.value][1]
                            else:
                                pass                    
                        if PHASE == 'main':
                                if c.value != 0:
                                    if c.value < 12:
                                        P.tiles.append(c.value)
                                        INVENTORY[c.value - 1].state = 'available'
                                    else:
                                        if c.value == 12:
                                            P.gold += 5000
                                        elif c.value == 13:
                                            P.gold += 3000
                                        elif c.value == 14:
                                            P.gold += 1000
                                        elif c.value == 15:
                                            P.gold += 500
                                    c.value = 0
                                    c.state = 'selected'
            elif menu.collidepoint(pos):
                if PHASE == 'setup':
                    pass # can add autofill and quite etc here
                if PHASE == 'main':
                    for i in INVENTORY:
                        if i.text_rect.collidepoint(pos): 
                            if i.state == 'unavailable':
                                pass
                            elif i.state == 'highlight':
                                match i.name:
                                    case 'DOUBLE':
                                        P.gold *= 2
                                    case 'BANK':
                                        P.bank = P.gold
                                        P.gold = 0
                                i.state = 'used'
                        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                if PHASE == 'setup':
                    for c in GRID:
                        if TILES and c.value == 0:
                            random.shuffle(TILES)
                            c.value = TILES.pop(0)
                            c.state = 'assigned'
                            c.image = TILE_DICT[c.value][1]


def title_screen():
    global PHASE
    PHASE = 'intro'
    while True:
        mouse_pos = pygame.mouse.get_pos()
        event_handler()
        screen.fill(BLACK)
        title_plank = pygame.transform.smoothscale(plank, (WINDOW_SIZE[0]//2, WINDOW_SIZE[1]//3))
        title_plank_rect = title_plank.get_rect()
        title_plank_rect.midtop = (WINDOW_SIZE[0]//2, WINDOW_SIZE[1]//5)
        screen.blit(title_plank, title_plank_rect)
        title = text.Text(screen, 'PIRATE GAME', title_plank_rect.center, size=WINDOW_SIZE[1]//10, colour=WHITE, anchor='center')
        for b in BUTTONS['TITLE']:
            if b.clicked:
                match b.msg:
                    case 'NEW GAME':
                        name = 'Steve' # Add graphical UI here
                        P = player.Player(name)
                        GAME = client.send(('new', P, None))
                        PHASE = 'setup'
                        print(f'created game {GAME.id}')
                        return P, GAME
                    case 'JOIN GAME':
                        name = 'Steve' # Add graphical UI here
                        P = player.Player(name)
                        GAME = client.send(('join', P, 'NXXI'))
                        PHASE = 'setup'
                        return P, GAME
                    case 'QUIT GAME':
                        pygame.quit()
                        sys.exit()
            if b.image_rect.collidepoint(mouse_pos):
                b.state = 'highlight'
            else:
                b.state = 'normal'
            b.draw()
        CLOCK.tick(60)
        pygame.display.flip()

 
    
def setup():
    global PHASE
    while True:
        event_handler()
        # Reset screen
        screen.fill(BLACK)
        pygame.draw.rect(screen, D_GRAY, grid)
        # Draw Title
        text.Text(screen, 'PIRATE GAME', (GRID_TOT_WIDTH + MENU_WIDTH//2, WINDOW_SIZE[1]//12),
                  colour=WHITE, size=WINDOW_SIZE[1]//12)
        # Draw the grid
        for c in GRID:
            c.draw()

        # Draw Menu
        if TILES:
            next_tile = TILES[0]
            if next_tile < 12:
                text.Text(screen, 'SET YOUR TRAPS...', (GRID_TOT_WIDTH + MENU_WIDTH //
                          2, WINDOW_SIZE[1]//4), colour=WHITE, size=WINDOW_SIZE[1]//20)
            else:
                text.Text(screen, 'HIDE YOUR LOOT...', (GRID_TOT_WIDTH + MENU_WIDTH //
                          2, WINDOW_SIZE[1]//4), colour=WHITE, size=WINDOW_SIZE[1]//20)
            img = TILE_DICT[next_tile][2]
            img = pygame.transform.smoothscale(img, (WINDOW_SIZE[1]//4, WINDOW_SIZE[1]//4))
            img_rect = img.get_rect()
            img_rect.center = menu.center
            screen.blit(img, img_rect)
            text.Text(screen, TILE_DICT[next_tile][0], (GRID_TOT_WIDTH +
                      MENU_WIDTH//2, 2*WINDOW_SIZE[1]//3), colour=WHITE, size=WINDOW_SIZE[1]//20)
        else:
            P.grid = [c.value for c in GRID]
            P.ready = True
            GAME.players[P.name] = P
            client.send(('update', GAME, None))
            PHASE = 'main'
            return
        CLOCK.tick(60)
        pygame.display.flip()    
    pygame.quit()


def waiting():
    global PHASE
    while not GAME.ready:
        mouse_pos = pygame.mouse.get_pos()
        event_handler()
        screen.fill(BLACK)
        text.Text(screen, 'Waiting for host to start the game...', screen.get_rect().center, colour=WHITE, size=60, anchor='center')
        CLOCK.tick(60)
        pygame.display.flip()  
    PHASE = 'main'

def main():
    global PHASE
    while True:
        mouse_pos = pygame.mouse.get_pos()
        event_handler()
        # Reset screen
        screen.fill(BLACK)
        pygame.draw.rect(screen, D_GRAY, grid)
        # Draw Title
        text.Text(screen, 'PIRATE GAME', (GRID_TOT_WIDTH + MENU_WIDTH//2, WINDOW_SIZE[1]//20),
                  colour=WHITE, size=50)
        # Draw the grid
        for c in GRID:
            c.draw()
        # Draw menu
        text.Text(screen, 'INVENTORY', (GRID_TOT_WIDTH + MENU_WIDTH//2,
                                   WINDOW_SIZE[1]//5), colour=GREEN, size=WINDOW_SIZE[1]//20)
        text.Text(screen, 'GOLD', (GRID_TOT_WIDTH + MENU_WIDTH//4,
                                   4*WINDOW_SIZE[1]//5), colour=GREEN, size=WINDOW_SIZE[1]//20)
        text.Text(screen, str(P.gold), (GRID_TOT_WIDTH + MENU_WIDTH//4,
                                   6*WINDOW_SIZE[1]//7), colour=WHITE, size=WINDOW_SIZE[1]//20)
        text.Text(screen, 'BANK', (GRID_TOT_WIDTH + 3*(MENU_WIDTH//4),
                                   4*WINDOW_SIZE[1]//5), colour=GREEN, size=WINDOW_SIZE[1]//20)
        text.Text(screen, str(P.bank), (GRID_TOT_WIDTH + 3*(MENU_WIDTH//4),
                                   6*WINDOW_SIZE[1]//7), colour=WHITE, size=WINDOW_SIZE[1]//20)
        for i in INVENTORY:
            match i.state:
                case 'unavailable':
                    pass
                case 'available':
                    if i.text_rect.collidepoint(mouse_pos):
                        i.state = 'highlight'
                case 'highlight':
                    if i.text_rect.collidepoint(mouse_pos):
                        pass
                    else:
                        i.state = 'available'
                case 'used':
                    pass
            i.draw()
        CLOCK.tick(60)
        pygame.display.flip()     
    pygame.quit()


# %% Game Loop
client = network.Network()
P, GAME = title_screen()
setup()
# waiting()
main()
