"""
server.py handles transfer of game state to the relevant client
"""
import socket
import threading
import pickle
import game

HOST = '127.0.0.1'
PORT = 57344
SERVER = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
SERVER.bind((HOST, PORT))
SERVER.listen()

clients = []
names = []
games = {}
    
def handle(client):
    while True:
        try:
            message = pickle.loads(client.recv(8192))
            print(f'message recieved: {message}')  
            command = message[0]
            data = message[1]
            extra_data = message[2]
            match command:
                case 'new':
                    g = game.Game()
                    g.add_player(data)
                    games[g.id] = g
                case 'join':
                    g = games[extra_data]
                    g.add_player(data)
                case 'update':
                    games[data.id] = data
                case _:
                    break
            client.send(pickle.dumps(g))
        except:
            index = clients.index(client)
            clients.remove(client)
            del names[index]
            client.close()
            print(f'{client} lost connection')
            break


def look_for_connections():
    while True:
        client, ADDR = SERVER.accept()
        print(f'connected to: {str(ADDR)}')
        message = True
        client.send(pickle.dumps(message))
        clients.append(client)
        
        thread = threading.Thread(target=handle, args=(client,))
        thread.start()

print('Server active, waiting for connections...')        
look_for_connections()


