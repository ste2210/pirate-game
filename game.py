import random
import string


class Game:
    def __init__(self):
        self.ready = False
        self.players = {}
        self.id = self.game_code()
        self.active_player = 0
        
    def game_code(self):
        return ''.join(random.choices(string.ascii_uppercase, k=4))
    
    def add_player(self, p):
        self.players[p.name] = p
        
    def get_active_player(self):
        if self.active_player == len(players)-1:
            self.active_player = 0
        else:
            self.active_player += 1
        